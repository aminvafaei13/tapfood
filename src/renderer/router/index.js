import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: require('@/components/LandingPage').default
    },
    {
      path: '/AddProduct',
      name: 'addProduct-page',
      component: require('@/components/Product/AddProduct').default
    },
    {
      path: '/Factor',
      name: 'factor-page',
      component: require('@/components/Product/Factor').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
