const { remote } = require('electron')
const Datastore = require('nedb-promises')
const path = require('path')

const dbFactory = (fileName) => Datastore.create({
  filename: path.join(remote.app.getPath('userData'), `/data/${fileName}`),
  timestampData: true,
  autoload: true
})

const db = {
  products: dbFactory('products.db')
}
module.exports = db
